"use strict"
/* 
Traffic Light CSS from
https://codepen.io/FlorinPop17/pen/ExayYWw

Data from
https://daten.berlin.de/tags/covid-19-erkrankungen
*/

// Set variables and Nodes

const baseUrl = "https://www.berlin.de/lageso/gesundheit/infektionskrankheiten/corona/tabelle-indikatoren-gesamtuebersicht/index.php/index/all.json"



let coronaFirstTen = []
let neuinfektionen

let hospitalisation
let itsBelegungList
let itsBelegung
let coronaToday = {}

let statsField = document.querySelector(".stats")
let trafficLightOneRed = document.querySelector("#newinfection-red")
let trafficLightOneYellow = document.querySelector("#newinfection-yellow")
let trafficLightOneGreen = document.querySelector("#newinfection-green")
let trafficLightOneValue = document.querySelector("#newinfection")


let trafficLightTwoRed = document.querySelector("#hospitalisation-red")
let trafficLightTwoYellow = document.querySelector("#hospitalisation-yellow")
let trafficLightTwoGreen = document.querySelector("#hospitalisation-green")
let trafficLightTwoValue = document.querySelector("#hospitalisation")

let trafficLightThreeRed = document.querySelector("#intensive-care-red")
let trafficLightThreeYellow = document.querySelector("#intensive-care-yellow")
let trafficLightThreeGreen = document.querySelector("#intensive-care-green")
let trafficLightThreeValue = document.querySelector("#intensive-care-beds")

// fetchData
fetch(`${baseUrl}`)
    .then(res => res.json())
    .then(data => {
                neuinfektionen = data.index[1]["7_tage_inzidenz"]
                hospitalisation = data.index[1]["7_tage_hosp_inzidenz"]
                itsBelegung = data.index[1]["its_belegung"]
                coronaFirstTen = data.index.filter((el, index) => index > 0 &&index < 9 )
                console.log(coronaFirstTen)
            }
        )
    .then(() => logDailyData())
    .catch(() => {
        let sorryMsg = document.createElement("div")
        sorryMsg.classList.add("error")
        sorryMsg.innerHTML = `<h2>Sorry, die API des LaGeSo liefert gerade keine aktuellen Daten. Versuchen Sie es später noch einmal.</h2>`
        statsField.appendChild(sorryMsg)
    })
// functions to create data sheet and call traffic light functions
function logDailyData () {
    coronaFirstTen = coronaFirstTen.slice(0,10)
    //calculateReproductionRate()
    //coronaLastTen.reverse()
    //console.log(coronaFirstTen)
    coronaFirstTen.forEach(item => {
        let newList = document.createElement("ul")
        newList.classList.add("daily-stats")
        newList.innerHTML = `<li>Datum: ${item.datum}</li>
        <li>Neue Fälle: ${item.neue_faelle}</li>
        <li>Neuinfektionen: ${item["7_tage_inzidenz"]}</li>
        <li>Hospitalisierungen: ${item["7_tage_hosp_inzidenz"]}</li>
        <li>Bettenbelegung: ${item.its_belegung}%</li>
        <hr>`
        statsField.appendChild(newList)
    })
    
    setTrafficLight(trafficLightOneRed, trafficLightOneYellow, trafficLightOneGreen, neuinfektionen, 100, 35)
    setTrafficLight(trafficLightTwoRed, trafficLightTwoYellow, trafficLightTwoGreen, hospitalisation, 8, 4)
    setTrafficLight(trafficLightThreeRed, trafficLightThreeYellow, trafficLightThreeGreen, itsBelegung, 20, 5)
    trafficLightOneValue.innerHTML = `${neuinfektionen}`
    trafficLightTwoValue.innerHTML = `${hospitalisation}`
    trafficLightThreeValue.innerHTML = `${itsBelegung}%`
}

// Traffic Light Control Center
function setTrafficLight(lightItemOne, lightItemTwo, lightItemThree, value, tresholdOne, tresholdTwo) {
    if (parseInt(value) >= tresholdOne) {
        lightItemOne.classList.add('red')
    } else if (value >= tresholdTwo) {
        lightItemTwo.classList.add('yellow')
    } else lightItemThree.classList.add('green')
}



// helper to calculate ReproductionRate
/*
function calculateReproductionRate() {
    let coronaLastTenRepro = [...coronaFirstTen]
    let coronaReproductionThree = coronaLastTenRepro.splice(5,3)
    averageReproRate = coronaReproductionThree
            .map(item => item["4_tage_r_wert_berlin_rki"])
            .reduce((acc, item) => acc + Number(item), 0)
    averageReproRate = (averageReproRate / 3).toPrecision(2)
}
*/